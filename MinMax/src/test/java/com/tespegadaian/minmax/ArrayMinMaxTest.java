/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tespegadaian.minmax;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author X220
 */
public class ArrayMinMaxTest {
    
    public ArrayMinMaxTest() {
    }

    /**
     * Test of getMinValue method, of class ArrayMinMax.
     */
    @Test
    public void testGetMinValue() {
        System.out.println("getMinValue");
        ArrayMinMax instance = new ArrayMinMax();
        int expResult = -25;
        int result = instance.getMinValue();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMaxValue method, of class ArrayMinMax.
     */
    @Test
    public void testGetMaxValue() {
        System.out.println("getMaxValue");
        ArrayMinMax instance = new ArrayMinMax();
        int expResult = 12345;
        int result = instance.getMaxValue();
        assertEquals(expResult, result);
    }
}
