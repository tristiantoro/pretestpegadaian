/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tespegadaian.validationobject;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author X220
 */
public class ValidationObject {
    private String input;
    private String output;
    
    public ValidationObject(String input){
        this.input = input;
        this.output = "";
    }
    
    public ValidationObject(){
        this.input = "";
        this.output = "";
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        if(isNumeric(input))
            output = input+" adalahangka";
        else
            output = input+" bukanangka";
        return output;
    }
    
    private boolean isNumeric(String input){
        this.input = input;
        return StringUtils.isNumeric(input);
    }
    
    
}
