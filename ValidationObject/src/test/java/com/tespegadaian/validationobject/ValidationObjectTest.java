/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tespegadaian.validationobject;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author X220
 */
public class ValidationObjectTest {
    
    public ValidationObjectTest() {
    }
    

    /**
     * Test of setInput method, of class ValidationObject.
     */
    @Test
    public void testSetInput() {
        System.out.println("setInput");
        String input = "12345";
        ValidationObject instance = new ValidationObject();
        instance.setInput(input);
    }

    /**
     * Test of getOutput method, of class ValidationObject.
     */
    @Test
    public void testGetOutput() {
        System.out.println("getOutput");
        ValidationObject instance = new ValidationObject("abc123");
        String expResult = "abc123 bukanangka";
        String result = instance.getOutput();
        assertEquals(expResult, result);
    }
    
}
