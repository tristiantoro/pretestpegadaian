package org.pegadaian.business.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;

public interface CubeService {

	Map<String,Object> getLatestRate();
	Map<String,Object> getLatestRateByDate(String tgl);
	Map<String,Object> getLatestAnalyze();
}
