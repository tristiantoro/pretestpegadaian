package org.pegadaian.business.service.impl;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.pegadaian.bean.ArrayMinMax;
import org.pegadaian.bean.Dataarrays;
import org.pegadaian.bean.jpa.DataarraysEntity;
import org.pegadaian.business.service.DataarraysService;
import org.pegadaian.business.service.mapping.DataarraysServiceMapper;
import org.pegadaian.data.repository.DataarraysRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class DataarraysServiceImpl implements DataarraysService {
	
	@Resource
	private DataarraysRepository dataarraysRepository;
	
	@Resource DataarraysServiceMapper dataarraysServiceMapper;

	@Override
	public Dataarrays findById(Integer id) {
		DataarraysEntity entity = dataarraysRepository.findOne(id);
		return dataarraysServiceMapper.mapDataarraysEntityToDataarrays(entity);
	}

	@Override
	public List<Dataarrays> findAll() {
		Iterable<DataarraysEntity> entities = dataarraysRepository.findAll();
		List<Dataarrays> beans = new ArrayList<Dataarrays>();
		for(DataarraysEntity ent : entities) {
			beans.add(dataarraysServiceMapper.mapDataarraysEntityToDataarrays(ent));
		}
		return beans;
	}

	@Override
	public Dataarrays create(Dataarrays ent) {
		DataarraysEntity entity = new DataarraysEntity();
		dataarraysServiceMapper.mapDataarraysToDataarraysEntity(ent, entity);
		DataarraysEntity entitySaved = dataarraysRepository.save(entity);
		return dataarraysServiceMapper.mapDataarraysEntityToDataarrays(entitySaved);
	}

	@Override
	public void delete() {
		dataarraysRepository.deleteAll();
	}

	@Override
	public Map<String, Object> getHitungAngka() {
		List<Dataarrays> rs = findAll();
		List<Integer> rsint = new ArrayList<Integer>();
		for (Dataarrays dataarrays : rs) {
			rsint.add(dataarrays.getData());
		}
		ArrayMinMax amm = new ArrayMinMax(rsint.stream().mapToInt(i->i).toArray());
		Map<String, Object> maprs = new HashMap<String, Object>();
		maprs.put("angka_terbesar", amm.getMaxValue());
		maprs.put("angka_terkecil", amm.getMinValue());
		return maprs;
	}

}
