package org.pegadaian.business.service.impl;

import java.util.List;
import java.util.Map;

import org.pegadaian.business.service.CubeService;
import org.pegadaian.business.service.dao.CubeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class CubeServiceImpl implements CubeService {
	
	 @Autowired
	    CubeDAO cubeDAO;

	public Map<String,Object> getLatestRate() {
		return cubeDAO.getLatest();
	}

	@Override
	public Map<String, Object> getLatestRateByDate(String tgl) {
		return cubeDAO.getLatestByDate(tgl);
	}

	@Override
	public Map<String, Object> getLatestAnalyze() {
		return cubeDAO.getAnalyze();
	}

}
