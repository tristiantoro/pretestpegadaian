/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pegadaian.business.service.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pegadaian.bean.Cube;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author X220
 */
@Repository("cubedao")
public class CubeDAO {
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public int saveCube(Cube e) {
		String query = "insert into cube (timer,currency,rate) values('" + e.getTime() + "','" + e.getCurrency() + "',"
				+ e.getRate() + ")";
		return jdbcTemplate.update(query);
	}

	public int deleteAllCube() {
		String query = "delete from cube ";
		return jdbcTemplate.update(query);
	}
	
	
	public Map<String,Object> getLatest(){
		Map<String,Object> result = new HashMap<String, Object>();
		List<Map<String,Object>> subResult = new ArrayList<Map<String,Object>>();
		String sql = "SELECT * FROM `cube` cub\r\n" + 
				"inner join (select max(timer) as timer from cube) as tmp on tmp.timer = cub.timer\r\n" + 
				"order by cub.rate";
		List<Map<String,Object>> rs = jdbcTemplate.queryForList(sql);
		Map<String,Object> map = new HashMap<String, Object>();
		for(Map<String,Object> mp:rs) {
			map.put(mp.get("currency").toString(), mp.get("rate"));
		}
		result.put("base", "EUR");
		result.put("rates",map);
		return  result;
	}
	
	public Map<String,Object> getLatestByDate(String date){
		Map<String,Object> result = new HashMap<String, Object>();
		String sql = "SELECT * FROM `cube` cub\r\n" + 
				" where timer='"+date+"' " + 
				"order by cub.rate";
		List<Map<String,Object>> rs = jdbcTemplate.queryForList(sql);
		Map<String,Object> map = new HashMap<String, Object>();
		for(Map<String,Object> mp:rs) {
			map.put(mp.get("currency").toString(), mp.get("rate"));
		}
		result.put("base", "EUR");
		result.put("rates",map);
		return  result;
	}
	
	public Map<String,Object> getAnalyze(){
		Map<String,Object> result = new HashMap<String, Object>();
		List<Map<String,Object>> subResult = new ArrayList<Map<String,Object>>();
		String sql = "select currency,sum(max) as max,sum(min) as min,sum(avg) as avg from (\r\n" + 
				"select currency,MAX(rate) as max,0 as min,0 as avg from cube group by currency\r\n" + 
				"union all\r\n" + 
				"select currency,0 as max,MIN(rate) as min,0 as avg from cube group by currency\r\n" + 
				"union all\r\n" + 
				"select currency,0 as max,0 as min,AVG(rate) as avg from cube group by currency\r\n" + 
				") as tmp group by currency";
		List<Map<String,Object>> rs = jdbcTemplate.queryForList(sql);
		for(Map<String,Object> mp:rs) {
			Map<String,Object> map = new HashMap<String, Object>();
			Map<String,Object> min = new HashMap<String, Object>();
			min.put("min", mp.get("min"));
			min.put("max", mp.get("max"));
			min.put("avg", mp.get("avg"));
			map.put(mp.get("currency").toString(),min);
			subResult.add(map);
		}
		result.put("base", "EUR");
		result.put("rates_analyze",subResult);
		return  result;
	}
	
}
