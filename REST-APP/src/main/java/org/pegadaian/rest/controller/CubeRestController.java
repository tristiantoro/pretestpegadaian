package org.pegadaian.rest.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.pegadaian.bean.Dataarrays;
import org.pegadaian.business.service.CubeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class CubeRestController {
	
	@Resource
	CubeService cubeService;
	
	@RequestMapping( value="/rates/latest",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Map<String,Object> findAll() {
		return cubeService.getLatestRate();
	}
	
	@RequestMapping( value="/rates/{tgl}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Map<String,Object> findAllByDate(@PathVariable("tgl")String date) {
		return cubeService.getLatestRateByDate(date);
	}
	
	@RequestMapping( value="/rates/analyze",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Map<String,Object> findAllAnalyze() {
		return cubeService.getLatestAnalyze();
	}

}
