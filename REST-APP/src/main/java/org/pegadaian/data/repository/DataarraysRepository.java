package org.pegadaian.data.repository;

import org.pegadaian.bean.jpa.DataarraysEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DataarraysRepository extends JpaRepository<DataarraysEntity, Integer>, JpaSpecificationExecutor<DataarraysEntity>  {

}
