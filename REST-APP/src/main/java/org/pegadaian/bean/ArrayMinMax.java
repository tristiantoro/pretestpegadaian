/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pegadaian.bean;

import com.google.common.primitives.Ints;
import java.util.Arrays;

/**
 *
 * @author X220
 */
public class ArrayMinMax {
    private final int[] dataArray;

    public ArrayMinMax() {
        this.dataArray = new int[]{-5, 12, 11, -25, 2, 12345};
    }
    
    public ArrayMinMax(int[] data) {
        this.dataArray = data;
    }
    
    public int getMinValue(){
        int min = Ints.min(dataArray);
        return min;
    }
    
    public int getMaxValue(){
        int max = Ints.max(dataArray);
        return max;
    }
}
