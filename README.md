# PreTestPegadaian

**Live Running Application (VPS)**
# ***http://web.tristiantoro.site:50197/PegadaianTest/***
<br>
Langkah - Langkah Menjalankan Aplikasi :<br>
1. Download atau Clone Repository.<br>
2. Jalankan IDE JAVA (Netbeans, Eclipse, Visual Code)<br>
3. Build dan deploy project tiap folder<br>
    *  Folder Minmax => Jawaban soal programming nomor 1<br>
    *  Folder CountChars => Jawaban soal programming nomor 3<br>
    *  Folder ValidationObject => Jawaban soal programming nomor 4<br>
    *  Folder XMLServices => Jawaban soal programming nomor 5<br>
    *  Folder REST-APP => Jawaban soal programming nomor 2, 5, 6, 7, 8<br>
4. Run project<br>
<br>
<br>

**Untuk Application REST jalankan file tespegadaian.sql untuk import DB.