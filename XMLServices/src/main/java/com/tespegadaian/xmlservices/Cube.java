/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tespegadaian.xmlservices;

/**
 *
 * @author X220
 */
public class Cube {
    
    private String time;
    private String currency;
    private double rate;

    public Cube(String time, String currency, double rate) {
        this.time = time;
        this.currency = currency;
        this.rate = rate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Cube{" + "time=" + time + ", currency=" + currency + ", rate=" + rate + '}';
    }
    
    
    
}
