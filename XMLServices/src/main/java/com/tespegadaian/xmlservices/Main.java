/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tespegadaian.xmlservices;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author X220
 */
public class Main {

    public static void main(String[] args) throws SAXException, ParserConfigurationException, MalformedURLException, IOException {
        List<Cube> listCube = new ArrayList<Cube>();
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new URL("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml").openStream());
        doc.getDocumentElement().normalize();
        NodeList nlist = doc.getElementsByTagName("Cube");
        int countTime = 0;
        String time = "";
        for (int i = 1; i < nlist.getLength(); i++) {
            Node nNode = nlist.item(i);
            if (nNode instanceof Element) {
                Element eElement = (Element) nNode;
                if (!eElement.getAttribute("time").equals("")) {
                    time = eElement.getAttribute("time");
                } else {
                    Cube cube = new Cube(time, eElement.getAttribute("currency"), Double.parseDouble(eElement.getAttribute("rate")));
                    listCube.add(cube);
                }
            }

        }
        CubeDAO dao = (CubeDAO) ctx.getBean("cubedao");
        for (Cube c : listCube) {
            System.out.println(c.toString());
            int status = dao.saveCube(new Cube(c.getTime(), c.getCurrency(), c.getRate()));
            System.out.println(status);
        }

    }
}
